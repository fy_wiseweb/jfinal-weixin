/*
 * Copyright (c) 2011-2021, L.cm (596392912@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import com.jfinal.weixin.sdk.utils.HttpUtils;
import com.jfinal.weixin.sdk.utils.JsonUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 发布接口
 * <p>
 * 文档地址：https://developers.weixin.qq.com/doc/offiaccount/Publish/Publish.html
 *
 * @author L.cm
 */
public class FreePublishApi {
    public static final String API_PREFIX = "https://api.weixin.qq.com/cgi-bin/freepublish/";

    /**
     * 发布接口
     *
     * @param mediaId mediaId
     * @return ApiResult
     */
    public static ApiResult submit(String mediaId) {
        String url = API_PREFIX + "submit?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, String> data = new HashMap<>();
        data.put("media_id", mediaId);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

    /**
     * 获取发布接口
     *
     * @param publishId publishId
     * @return ApiResult
     */
    public static ApiResult get(String publishId) {
        String url = API_PREFIX + "get?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, String> data = new HashMap<>();
        data.put("publish_id", publishId);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

    /**
     * 删除发布
     *
     * @param articleId 文章id
     * @param index     index
     * @return ApiResult
     */
    public static ApiResult delete(String articleId, int index) {
        String url = API_PREFIX + "delete?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, Object> data = new HashMap<>();
        data.put("article_id", articleId);
        data.put("index", index);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

    /**
     * 获取发布文章
     *
     * @param articleId articleId
     * @return ApiResult
     */
    public static ApiResult getArticle(String articleId) {
        String url = API_PREFIX + "getarticle?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, String> data = new HashMap<>();
        data.put("article_id", articleId);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

    /**
     * 获取成功发布列表
     *
     * @param offset offset
     * @param count  count
     * @return ApiResult
     */
    public static ApiResult getBatch(int offset, int count) {
        return getBatch(offset, count, false);
    }

    /**
     * 获取成功发布列表
     *
     * @param offset    offset
     * @param count     count 返回素材的数量，取值在1到20之间
     * @param noContent noContent
     * @return ApiResult
     */
    public static ApiResult getBatch(int offset, int count, boolean noContent) {
        String url = API_PREFIX + "batchget?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, Object> data = new HashMap<>();
        data.put("offset", offset);
        data.put("count", count);
        data.put("no_content", noContent ? 1 : 0);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

}
